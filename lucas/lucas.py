#El 18-th número de Lucas,
#el número de Lucas más cercano a 1000, y
#el primer número de Lucas más grande que 100


# recursive function 
def lucas(n) : 
	if (n == 0) : 
		return 2
	if (n == 1) : 
		return 1	
	return lucas(n - 1) + lucas(n - 2) 

def buscarlucas(a,b):    
    for i in range(b):
        number = lucas(i)
        # Si es mayor o igual
        if number >= b:
            #Cercano
            if (a=='c') :
                number2 = lucas(i-1)
                if (number-b)>(b-number2):
                    print(i,"-th")
                    break
                else:
                    print(i-1,"-th")
                    break
            #Else - Mayor  
            else:
                print("es", i,"-th")
                break
        
# Main
#numero 18 lucas
n = 18
print("18-th",lucas(n)) 
# cercano de 1000
cerca= "c"
mil = 1000
print("cercano a 1000 ")
buscarlucas(cerca, mil)
# mas grande de 100
print("mas grande que 100")
mayor= "m"
cien = 100
buscarlucas(mayor, cien)
